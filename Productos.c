/*
	Author: EduardoQSR
	Date: February, 24th, 2020 
*/

#include<stdio.h>
#include <stdlib.h>

#define String char[30]

typedef struct
{
	int codigo;
	char descripcion[41];
	float precio;
}producto;

 producto cargar()
{
	producto *product = (producto *) malloc(sizeof(producto));
	//struct producto product;
	printf("--------------------------------------------\n");
	printf("Ingrese codigo de producto: ");
	scanf("%i",&product->codigo);
	fflush(stdin);
	printf("Ingrese nombre del producto: ");
	scanf("%s",&product->descripcion);
	printf("Ingrese precio de producto: ");
	scanf("%f",&product->precio);
	//fgets(product.descripcion, 30, stdin);
	return *product;
};

void imprimir(producto product)
{
	printf("Codigo:%i\n", product.codigo);
	printf("Nombre: %s\n", product.descripcion);
	printf("Precio: %0.2f\n", product.precio);
	printf("--------------------------------------------\n");
}

int main()
{
	//figura *area = (figura *) malloc(sizeof(figura));
	producto *productoUno = (producto *) malloc(sizeof(producto));
	producto *productoDos = (producto *) malloc(sizeof(producto));
	//struct producto productoUno, productoDos;
	printf("--------------------------------------------\n");
	printf("---------------INFORMACION------------------\n");
	*productoUno = cargar();
	*productoDos = cargar();
	printf("--------------------------------------------\n");
	printf("\n\n\n\n\n--------------------------------------------\n");
	printf("--------------DATOS DE PRODUCTO-------------\n");
	printf("--------------------------------------------\n");
	imprimir(*productoUno);
	imprimir(*productoDos);
	//getch();
	return 0;
}
